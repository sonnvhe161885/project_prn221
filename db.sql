USE [master]
GO
/****** Object:  Database [PROJECTPRN221]    Script Date: 3/23/2024 1:01:59 PM ******/
CREATE DATABASE [PROJECTPRN221]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PROJECTPRN221', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.GROOT\MSSQL\DATA\PROJECTPRN221.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PROJECTPRN221_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.GROOT\MSSQL\DATA\PROJECTPRN221_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [PROJECTPRN221] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PROJECTPRN221].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PROJECTPRN221] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET ARITHABORT OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PROJECTPRN221] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PROJECTPRN221] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PROJECTPRN221] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET  ENABLE_BROKER 
GO
ALTER DATABASE [PROJECTPRN221] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PROJECTPRN221] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PROJECTPRN221] SET  MULTI_USER 
GO
ALTER DATABASE [PROJECTPRN221] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PROJECTPRN221] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PROJECTPRN221] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PROJECTPRN221] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PROJECTPRN221] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PROJECTPRN221] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [PROJECTPRN221] SET QUERY_STORE = OFF
GO
USE [PROJECTPRN221]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/23/2024 1:02:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Id] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Address] [nvarchar](max) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](max) NULL,
	[RoleId] [int] NULL,
	[Password] [nvarchar](max) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 3/23/2024 1:02:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 3/23/2024 1:02:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[ProductId] [nvarchar](50) NULL,
	[Quantity purchased] [int] NULL,
	[Quantity Sell] [int] NULL,
	[Price] [float] NULL,
 CONSTRAINT [PK_OrderDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 3/23/2024 1:02:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](50) NOT NULL,
	[CustomerName] [nvarchar](50) NULL,
	[OrderDate] [datetime] NULL,
	[Amount] [float] NULL,
	[StatusId] [int] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 3/23/2024 1:02:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 3/23/2024 1:02:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Quantity] [int] NULL,
	[CategoryId] [int] NULL,
	[Purchase price] [float] NULL,
	[Retail price] [float] NULL,
	[Image] [nvarchar](max) NULL,
	[Date] [datetime] NULL,
	[Stock] [int] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 3/23/2024 1:02:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Account] ([Id], [UserName], [Address], [Phone], [Email], [RoleId], [Password]) VALUES (N'AD1', N'admin', N'VietNam', N'0123456789', NULL, 1, N'123')
INSERT [dbo].[Account] ([Id], [UserName], [Address], [Phone], [Email], [RoleId], [Password]) VALUES (N'ST1', N'staff1', N'VN', N'0123456789', NULL, 2, N'123')
INSERT [dbo].[Account] ([Id], [UserName], [Address], [Phone], [Email], [RoleId], [Password]) VALUES (N'ST2', N'staff2', N'VN', N'099999999', NULL, 2, N'123')
INSERT [dbo].[Account] ([Id], [UserName], [Address], [Phone], [Email], [RoleId], [Password]) VALUES (N'ST3', N'staff3', N'VN', N'099999999', NULL, 2, N'123')
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([Id], [Name]) VALUES (1, N'9999')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (2, N'999(24K)')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (3, N'18K')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (4, N'14K')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (5, N'10K')
INSERT [dbo].[Category] ([Id], [Name]) VALUES (6, N'99')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderDetail] ON 

INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (1, 2, N'KBv', 0, 1, 6320000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (2, 2, N'PLTv', 0, 1, 6440000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (3, 7, N'KBv', 0, 1, 6430000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (4, 7, N'PLTv', 0, 1, 6440000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (5, 7, N'PNJm', 0, 1, 6440000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (6, 8, N'KBv', 0, 2, 6430000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (7, 8, N'PLTv', 0, 1, 6440000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (8, 9, N'KBv', 0, 2, 6430000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (9, 10, N'KBv', 0, 2, 6430000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (10, 11, N'KBv', 10, 0, 6320000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (11, 12, N'KBv', 0, 3, 6430000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (12, 13, N'KBv', 1, 0, 6320000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (13, 14, N'KBv', 0, 3, 6430000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (14, 15, N'KBv', 0, 1, 6430000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (15, 16, N'KBv', 0, 1, 6430000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (16, 17, N'PLTv', 0, 2, 6440000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (17, 18, N'SJCm', 0, 1, 7820000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (18, 19, N'V416', 1, 0, 2533000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (19, 20, N'PNJm', 0, 1, 6440000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (20, 21, N'KBv', 2, 0, 6320000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (21, 22, N'V585', 0, 1, 3753000)
INSERT [dbo].[OrderDetail] ([Id], [OrderId], [ProductId], [Quantity purchased], [Quantity Sell], [Price]) VALUES (22, 23, N'VNT1', 1, 0, 6310000)
SET IDENTITY_INSERT [dbo].[OrderDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (2, N'ST1', N'dang hoan', CAST(N'2024-02-24T00:00:00.000' AS DateTime), 12870000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (7, N'ST1', N'', CAST(N'2024-02-25T17:20:40.857' AS DateTime), 19310000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (8, N'ST2', N'tony', CAST(N'2024-02-25T20:31:03.353' AS DateTime), 19300000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (9, N'ST2', N'stack', CAST(N'2024-02-25T21:15:23.107' AS DateTime), 12860000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (10, N'ST1', N'abc', CAST(N'2024-02-25T21:56:04.620' AS DateTime), 12860000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (11, N'ST1', N'hoan', CAST(N'2024-02-25T22:44:01.427' AS DateTime), 63200000, 2)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (12, N'ST1', N'', CAST(N'2024-02-25T23:01:01.943' AS DateTime), 19290000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (13, N'ST1', N'hi', CAST(N'2024-02-25T23:05:08.167' AS DateTime), 6320000, 2)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (14, N'ST1', N'vu', CAST(N'2024-02-25T23:19:50.797' AS DateTime), 19290000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (15, N'ST1', N'', CAST(N'2024-02-26T21:36:17.383' AS DateTime), 6430000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (16, N'ST1', N'hoan', CAST(N'2024-02-26T21:40:34.323' AS DateTime), 6430000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (17, N'ST1', N'abc', CAST(N'2024-02-26T22:01:09.393' AS DateTime), 12880000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (18, N'ST1', N'hoan', CAST(N'2024-02-26T22:04:33.400' AS DateTime), 7820000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (19, N'ST1', N'vu', CAST(N'2024-02-26T22:04:57.510' AS DateTime), 2533000, 2)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (20, N'ST1', N'hihi', CAST(N'2024-02-27T22:43:35.083' AS DateTime), 6440000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (21, N'ST2', N'', CAST(N'2024-02-29T10:18:27.897' AS DateTime), 12640000, 2)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (22, N'ST2', N'hoan', CAST(N'2024-03-23T10:42:06.560' AS DateTime), 3753000, 1)
INSERT [dbo].[Orders] ([Id], [UserId], [CustomerName], [OrderDate], [Amount], [StatusId]) VALUES (23, N'ST2', N'hoan', CAST(N'2024-03-23T10:42:24.083' AS DateTime), 6310000, 2)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (1, N'Bán Ra')
INSERT [dbo].[OrderStatus] ([Id], [Name]) VALUES (2, N'Mua Vào')
GO
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'KBv', N'Vàng Kim Bảo 9999', 1, 1, 6320000, 6430000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 101)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'PLTv', N'Vàng Phúc Lộc Tài 9999', 1, 1, 6320000, 6440000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 98)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'PNJm', N'Vàng miếng PNJ 9999', 1, 1, 6320000, 6440000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 99)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'PNJn', N'Nhẫn Trơn PNJ 9999', 1, 1, 6320000, 6430000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 100)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'SJCm', N'Vàng miếng SJC 9999', 1, 1, 7600000, 7820000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 99)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'TEST', N'TEST3', 1, 6, 6536000, 6636000, NULL, CAST(N'2024-03-23T10:24:24.313' AS DateTime), 10)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'V416', N'Vàng 416 ', 1, 5, 2533000, 2673000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 101)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'V585', N'Vàng 585', 1, 4, 3613000, 3753000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 99)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'V750', N'Vàng 750', 1, 3, 4668000, 4808000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 100)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'VNT1', N'Vàng nữ trang 9999', 1, 1, 6310000, 6390000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 101)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'VNT2', N'Vàng nữ trang 999', 1, 2, 6304000, 6384000, NULL, CAST(N'2024-03-18T00:00:00.000' AS DateTime), 100)
INSERT [dbo].[Products] ([Id], [Name], [Quantity], [CategoryId], [Purchase price], [Retail price], [Image], [Date], [Stock]) VALUES (N'VNT3', N'Vàng nữ trang 99', 1, 6, 6236000, 6336000, NULL, CAST(N'2024-03-23T10:26:20.227' AS DateTime), 100)
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [Name]) VALUES (1, N'Admin')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (2, N'Staff')
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Roles]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_Orders]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_Products]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Account] FOREIGN KEY([UserId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Account]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[OrderStatus] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_OrderStatus]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Category]
GO
USE [master]
GO
ALTER DATABASE [PROJECTPRN221] SET  READ_WRITE 
GO
