﻿using GoldManagement.Models;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace GoldManagement
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public readonly PROJECTPRN221Context _context;
        public MainWindow()
        {
            InitializeComponent();
            this.KeyDown += new KeyEventHandler(Window_KeyDown);
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                btnLogin_Click(sender, e);
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                DragMove();
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            PROJECTPRN221Context context = new PROJECTPRN221Context();
            string username = txtUser.Text.ToString();
            string password = txtPass.Password.ToString();
            var account = context.Accounts.FirstOrDefault(a => a.UserName == username && a.Password == password);
            if (account != null)
            {
                Session.Account = account;
                Session.Username = username;

                if (account.RoleId == 1)
                {
                    this.Hide();
                    AdminManager adminManager = new AdminManager();
                    adminManager.Show();
                }
                else
                {
                    this.Hide();
                    Home home = new Home();
                    home.Show();
                }
            }
            else
            {
                MessageBox.Show("Please enter username and password");
            }
        }

        public void resetFormLogin()
        {
            txtUser.Text = null;
            txtPass.Password = null;
        }
    }
}
